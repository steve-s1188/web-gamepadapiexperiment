// Namespace declaration
var gp = gp || {};

// Utils module
gp.Utils = (function(window, undefined) {
    
    // private parts
    var gamepads = {};
    
    function hasGamepadSupport() {
        return "getGamepads" in navigator;
    };

    function displayMessage(msg){
        $("#msg-prompt").text(msg);
    }

    // public API
    return {
        hasGamepadSupport: hasGamepadSupport,
        displayMessage: displayMessage
    }
})(window);

// Gamepad class
gp.Gamepad = function(gamepad){
    
    console.info('gamepad created', gamepad);
    
    this.id = gamepad.id;
    this.index = gamepad.index;
    this.mapping = gamepad.mapping;
    this.isConnected = gamepad.connected;
    this.buttons = gamepad.buttons;
    this.axes = gamepad.axes;
    this.lastUpdated = gamepad.timestamp;
}
gp.Gamepad.prototype.update = function(){
    this.id = navigator.getGamepads()[this.index].id;
    this.mapping = navigator.getGamepads()[this.index].mapping;
    this.isConnected = navigator.getGamepads()[this.index].connected;
    this.buttons = navigator.getGamepads()[this.index].buttons;
    this.axes = navigator.getGamepads()[this.index].axes;
    this.lastUpdated = navigator.getGamepads()[this.index].timestamp;
}

gp.Gamepad.prototype.toReport = function() {
    
    this.update();
    
    var html = "";
    html += "id: " + this.id + "<br />";
 
    for(var i=0;i < this.buttons.length;i++) {
        html += "Button "+ (i+1) +": ";
        if(this.buttons[i].pressed) html += " pressed";
        html += "<br />";
    }
 
    for(var i=0;i < this.axes.length; i+=2) {
        html += "Twiddle Stick " + (Math.ceil(i/2) + 1) + ": " + this.axes[i] + "," + this.axes[i+1]+  " <br />";
    }
    
    return html;
}

// GamepadPoller module
gp.GamepadPoller = (function(window, undefined) {
    
    // private parts
    var gamepads = {};
    var pollers = {};
    
    function hasActiveGamepad(){
        return Object.keys(gamepads).length > 0;
    }
    
    function addGamepad(gamepadObj){
        gp.Utils.displayMessage("Gamepad connected: " + gamepadObj.id);
        gamepads[gamepadObj.id] = gamepadObj;
        pollGamepad(gamepadObj);
    }
    
    function removeGamepad(id){
        delete gamepads[id];
        delete pollers[id];
    }
    
    function pollGamepad(gamepadObj){
        pollers[gamepadObj.id] = window.setInterval(function(){
            var html = gamepads[gamepadObj.id].toReport();
            $('#gamepad-report').html(html);
        }, 100);
    }
    
    // public API
    return {
        hasActiveGamepad: hasActiveGamepad,
        addGamepad: addGamepad,
        removeGamepad: removeGamepad
    }
})(window);

// GamepadMonitor module
gp.GamepadMonitor = (function(window, undefined) {
    
   /**
    * Initialize the gamepad support.
    */
    function initialize(){

        // check whether the browser even supports the gamepad
        if(!gp.Utils.hasGamepadSupport()){
            console.error("Browser does not support Gamepad API");
            return;
        }
        
        // show message
        gp.Utils.displayMessage("To begin using your gamepad, connect it and press any button.");

        // setup events
        $(window).on("gamepadconnected", gp.GamepadConnectedEvent);
        $(window).on("gamepaddisconnected", gp.GamepadDisconnectedEvent);
    
        // chrome (requires an interval to poll for gamepad)
        // todo - only setup based on browser type. 
        var checkGP = window.setInterval(function() {
            if(navigator.getGamepads()[0]) {
                if(!gp.GamepadPoller.hasActiveGamepad()) { // found - fire connected event
                    $(window).trigger("gamepadconnected");
                }               
                window.clearInterval(checkGP);
            }
        }, 500);
    }
    
    // Public API
    return {
        init: initialize
    }
})(window);

// GamepadConnectedEvent class
gp.GamepadConnectedEvent = function(evt){
    var pad = new gp.Gamepad(navigator.getGamepads()[0]);
    gp.GamepadPoller.addGamepad(pad)
}

// GamepadDisconnectedEvent class
gp.GamepadDisconnectedEvent = function(evt){
    gp.GamepadPoller.removeGamepad();
}

 // logic
 $(document).ready(function() {
     gp.GamepadMonitor.init();
 });
